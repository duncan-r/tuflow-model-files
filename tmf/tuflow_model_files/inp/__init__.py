
#
# Package constants
#

# Class types
ATTR = "AttrInput"
CF = "ControlFileInput"
COMMENT = "CommentInput"
DB = "DatabaseInput"
FILE = "FileInput"
GIS = "GisInput"
GRID = "GridInput"
SETTING = "SettingInput"
TIN = "TinInput"
# Default
INPUT = "Input"