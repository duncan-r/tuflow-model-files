from ._cf_run_state import ControlFileRunState
from ..abc.tef_base import TEFBase


class TEFRunState(ControlFileRunState, TEFBase):
    pass
