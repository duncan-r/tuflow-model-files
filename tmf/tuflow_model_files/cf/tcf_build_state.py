from ._cf_build_state import ControlFileBuildState
from ..abc.tcf_base import TCFBase


class TCFBuildState(ControlFileBuildState, TCFBase):
    pass
