#
# Package constants
#

# Class types
ADCF = "ADControlFile"
ECF = "EstryControlFile"
QCF = "QuadtreeControlFile"
TBC = "TuflowBoundaryControl"
TCF = "TuflowControlFile"
TEF = "TuflowEventFile"
TESF = "TuflowExternalStressFile"
TGC = "TuflowGeometryControl"
TOC = "TuflowOperatingControl"
TRD = "TuflowReadFile"
TRFC = "TuflowRainfallControl"
TSCF = "TuflowSWMMControl"
# Default
CONTROLFILE = "ControlFile"

SHORT_TYPES = {
    "ADControlFile": "ADCF",
    "EstryControlFile": "ECF",
    "QuadtreeControlFile": "QCF",
    "TuflowBoundaryControl": "TBC",
    "TuflowControlFile": "TCF",
    "TuflowEventFile": "TEF",
    "TuflowExternalStressFile": "TESF",
    "TuflowGeometryControl": "TGC",
    "TuflowOperatingControl": "TOC",
    "TuflowReadFile": "TRD",
    "TuflowRainfallControl": "TRFC",
    "TuflowSWMMControl": "TSCF",
    # Default
    "ControlFile": "",
}
