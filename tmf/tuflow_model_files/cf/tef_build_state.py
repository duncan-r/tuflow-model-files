from ._cf_build_state import ControlFileBuildState
from ..abc.tef_base import TEFBase


class TEFBuildState(ControlFileBuildState, TEFBase):
    pass