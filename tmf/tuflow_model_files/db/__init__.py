
#
# Package constants
#

# Class types
DATABASE = 'Database'
BC_DBASE = 'BCDatabase'
MAT = 'MaterialsDatabase'
PIT = 'PitInletDatabase'
RAINFALL = 'RainfallDatabase'
SOIL = 'SoilDatabase'
XS = 'CrossSectionDatabase'